\ProvidesClass{friggeri-cv-rishabs-mod}[2012/04/30 CV class]
\NeedsTeXFormat{LaTeX2e}

\DeclareOption{print}{\def\@cv@print{}}
\DeclareOption*{%
  \PassOptionsToClass{\CurrentOption}{article}%
}
\ProcessOptions\relax
\LoadClass{article}


%%%%%%%%%%
% Colors %
%%%%%%%%%%

\RequirePackage{xcolor}
\RequirePackage{longtable}
\definecolor{white}{RGB}{255,255,255}

\definecolor{darkgray}{HTML}{333333}
\definecolor{gray}{HTML}{4D4D4D}
\definecolor{lightgray}{HTML}{999999}

\definecolor{green}{HTML}{C2E15F}
\definecolor{orange}{HTML}{FDA333}
\definecolor{purple}{HTML}{D3A4F9}
\definecolor{red}{HTML}{FB4485}
\definecolor{blue}{HTML}{6CE0F1}
\definecolor{sbured}{HTML}{A00000}
\ifdefined\@cv@print
  \colorlet{green}{gray}
  \colorlet{orange}{gray}
  \colorlet{purple}{gray}
  \colorlet{red}{gray}
  \colorlet{blue}{gray}
  \colorlet{fillheader}{white}
  \colorlet{header}{gray}
\else
  \colorlet{fillheader}{white}
  \colorlet{header}{gray}
\fi
\colorlet{textcolor}{gray}
\colorlet{headercolor}{gray}

%%%%%%%%%
% Fonts %
%%%%%%%%%

% \RequirePackage[quiet]{fontspec}
% \RequirePackage[math-style=TeX,vargreek-shape=unicode]{unicode-math}
% 
% \newfontfamily\bodyfont[]{Helvetica Neue}
% \newfontfamily\thinfont[]{Helvetica Neue UltraLight}
% \newfontfamily\headingfont[]{Helvetica Neue Condensed Bold}
% 
% \defaultfontfeatures{Mapping=tex-text}
% \setmainfont[Mapping=tex-text, Color=textcolor]{Helvetica Neue Light}
% 
% \setmathfont{XITS Math}

%%% modified by Karol Kozioł for ShareLaTex use
\RequirePackage[quiet]{fontspec}
\RequirePackage[math-style=TeX,vargreek-shape=unicode]{unicode-math}

\newfontfamily\bodyfont
[BoldFont=texgyreheros-bold.otf,
ItalicFont=texgyreheros-italic.otf,
BoldItalicFont=texgyreheros-bolditalic.otf]
{texgyreheros-regular.otf}
\newfontfamily\thinfont[]{Lato-Light.ttf}
\newfontfamily\headingfont[]{texgyreheros-bold.otf}

\defaultfontfeatures{Mapping=tex-text}
\setmainfont
[Mapping=tex-text, Color=textcolor,
BoldFont=texgyreheros-bold.otf,
ItalicFont=texgyreheros-italic.otf,
BoldItalicFont=texgyreheros-bolditalic.otf
]
{texgyreheros-regular.otf}

\setmathfont{texgyreheros-regular.otf}
%%%

%%%%%%%%%%
% Header %
%%%%%%%%%%

\RequirePackage{tikz}

\newcommand{\rolefont}{%
  \fontsize{14pt}{24pt}\selectfont%
  \thinfont%
  \color{white}%
}

\newcommand{\header}[3]{%
  \begin{tikzpicture}[remember picture,overlay]
    \node [rectangle, fill=fillheader, anchor=north, minimum width=20cm, minimum height=4cm] (box) at (current page.north){};
    \node [anchor=center] (name) at (box) {%
      \fontsize{40pt}{72pt}\color{header}%
      {\thinfont #1}{\bodyfont  #2}
    };
    \node [anchor=north] at (name.south) {%
      \fontsize{10pt}{24pt}\color{header}%
      \thinfont #3%
    };
  \end{tikzpicture}
  \vspace{2.5cm}
  \vspace{-2\parskip}
}

%%%%%%%%%%%%%
% Footer		%
%%%%%%%%%%%%%
\newcommand{\footer}[2]{
\renewcommand{\headrulewidth}{0pt} % no line in header area
\fancyfoot{} % clear all footer fields
\rfoot{{\footnotesize\addfontfeature{Color=lightgray} #1}\hfill{\footnotesize\addfontfeature{Color=lightgray} #2}} 
}

%%%%%%%%%%%%%
% Structure %
%%%%%%%%%%%%%
\RequirePackage{parskip}

\newcounter{colorCounter}
%\def\@sectioncolor#1#2#3{%
\def\@sectioncolor{%
  {%
    \color{%
      \ifcase\value{colorCounter}%
        sbured\else%
        sbured\fi%
    } %
  }%
  \stepcounter{colorCounter}%
}

\renewcommand{\section}[1]{
  \par\vspace{\parskip}
  {%
    \LARGE\headingfont\color{headercolor}%
    \@sectioncolor #1%
  }
  \par\vspace{\parskip}
}

\renewcommand{\subsection}[1]{
  \par\vspace{.5\parskip}%
  {%
  \large\headingfont\color{headercolor} #1%
  }
  \par\vspace{.25\parskip}%
}

\pagestyle{empty}


%%%%%%%%%%%%%%%%%%%%
% List environment %
%%%%%%%%%%%%%%%%%%%%

\setlength{\tabcolsep}{0pt}
\newenvironment{entrylist}{%
  \begin{longtable}{@{\extracolsep{\fill}}ll}
}{%
  \end{longtable}\vspace{-.25in}
}
\renewcommand{\bfseries}{\headingfont\color{headercolor}}
\newcommand{\entry}[4]{%
  #1&\parbox[t]{14cm}{%
    \textbf{#2}%
    \hfill%
    {\footnotesize\addfontfeature{Color=lightgray} #3}\\%
    #4\vspace{.15cm}%
  }\\}
%%%%%%%%%%%%%%%%%%%%
% All environments: publist/pub/selected, education/school, work/org, talks/talk,
% teaching/class, references/referee.
%%%%%%%%%%%%%%%%%%%%

\setlength{\tabcolsep}{0pt}
\newenvironment{publist}{%
  \begin{longtable}{@{\extracolsep{\fill}}ll}
}{%
  \end{longtable}\vspace{-.25in}
}
\renewcommand{\bfseries}{\headingfont\color{headercolor}}
\newcommand{\pub}[4]{%
  \parbox[t]{3cm}{\footnotesize\addfontfeature{Color=lightgray} #1}&\parbox[t]{15.5cm}{%
    \textbf{#2}%
    \hfill%
    {\footnotesize\addfontfeature{Color=lightgray} #3}\\%
    #4\vspace{.25cm}%
  }\\}
  
\newcommand{\selected}[4]{%
  \parbox[t]{3cm}{\footnotesize{\addfontfeature{Color=sbured}#1}}&\parbox[t]{15.5cm}{%
    \textbf{\addfontfeature{Color=sbured}#2}%
    \hfill%
    {\footnotesize\addfontfeature{Color=lightgray} #3}\\%
    \addfontfeature{Color=sbured}#4\vspace{.25cm}%
  }\\}
  
  
\newenvironment{education}{%
  \begin{longtable}{@{\extracolsep{\fill}}ll}
}{%
  \end{longtable}\vspace{-.25in}
}
\renewcommand{\bfseries}{\headingfont\color{headercolor}}
\newcommand{\school}[4]{%
  \parbox[t]{4cm}{\footnotesize\addfontfeature{Color=lightgray} #1}&\parbox[t]{14cm}{%
    \textbf{#2}%
    \hfill%
    {\footnotesize\addfontfeature{Color=lightgray} #3}\\%
    #4\vspace{.25cm}%
  }\\}


\newenvironment{work}{%
  \begin{longtable}{@{\extracolsep{\fill}}ll}
}{%
  \end{longtable}\vspace{-.25in}
}
\renewcommand{\bfseries}{\headingfont\color{headercolor}}
\newcommand{\org}[4]{%
  \parbox[t]{4cm}{\footnotesize\addfontfeature{Color=lightgray} #1}&\parbox[t]{14cm}{%
    \textbf{#2}%
    \hfill%
    {\footnotesize\addfontfeature{Color=lightgray} #3}\\%
    #4\vspace{.25cm}%
  }\\}


\newenvironment{talks}{%
}{%
}
\renewcommand{\bfseries}{\headingfont\color{headercolor}}
\newcommand{\talk}[2]{\textbf{#1}\\#2\vspace{.25cm}\\}

\newenvironment{services}{%
  \begin{longtable}{@{\extracolsep{\fill}}ll}
}{%
  \end{longtable}\vspace{-.25in}
}
\renewcommand{\bfseries}{\headingfont\color{headercolor}}
\newcommand{\service}[2]{\parbox[t]{4cm}{\footnotesize\addfontfeature{Color=lightgray}#1}& \parbox[t]{14cm}{#2}\vspace{.25cm}\\}


\setlength{\tabcolsep}{0pt}
\newenvironment{grants}{%
  \begin{longtable}{@{\extracolsep{\fill}}ll}
}{%
  \end{longtable}\vspace{-.25in}
}
\renewcommand{\bfseries}{\headingfont\color{headercolor}}
\newcommand{\grant}[2]{\parbox[t]{4cm}{\footnotesize\addfontfeature{Color=lightgray}#1}& \parbox[t]{14cm}{#2}\vspace{.25cm}\\}


\setlength{\tabcolsep}{0pt}
\newenvironment{teaching}{%
  \begin{longtable}{@{\extracolsep{\fill}}ll}
}{%
  \end{longtable}\vspace{-.25in}
}
\renewcommand{\bfseries}{\headingfont\color{headercolor}}
\newcommand{\class}[4]{%
  \parbox[t]{4cm}{\footnotesize\addfontfeature{Color=lightgray} #1}&\parbox[t]{14cm}{%
    \textbf{#2}%
    \hfill%
    {\footnotesize\addfontfeature{Color=lightgray} #3}\\%
    #4\vspace{.25cm}%
  }\\}
  
  
\setlength{\tabcolsep}{0pt}
\newenvironment{references}{%
  \begin{longtable}{@{\extracolsep{\fill}}ll}
}{%
  \end{longtable}\vspace{-.25in}
}
\renewcommand{\bfseries}{\headingfont\color{headercolor}}
\newcommand{\referee}[3]{%
  \parbox[t]{4cm}{\textbf{#1}}&\parbox[t]{14cm}{{#2}\hfill    {\Letter ~ #3}\vspace{.25cm}  }\\}

%%%%%%%%%%%%%%
% Side block %
%%%%%%%%%%%%%%

\RequirePackage[absolute,overlay]{textpos}
\setlength{\TPHorizModule}{1cm}
\setlength{\TPVertModule}{1cm}
\newenvironment{aside}{%
  \let\oldsection\section
  \renewcommand{\section}[1]{
    \par\vspace{\baselineskip}{\Large\headingfont\color{headercolor} ##1}
  }
  \begin{textblock}{3.6}(1.5, 4.33)
  \begin{flushright}
  \obeycr
}{%
  \restorecr
  \end{flushright}
  \end{textblock}
  \let\section\oldsection
}

%%%%%%%%%%%%%%%%
% Bibliography %
%%%%%%%%%%%%%%%%

\RequirePackage[style=verbose, maxnames=99, sorting=ydnt]{biblatex}

\DeclareFieldFormat[article]{title}{#1\par}
\DeclareFieldFormat[inproceedings]{title}{#1\par}
\DeclareFieldFormat[misc]{title}{#1\par}
\DeclareFieldFormat[report]{title}{#1\par}

\DeclareBibliographyDriver{article}{%
  \printfield{title}%
  \newblock%
  \printnames{author}%
  \par%
  \newblock%
  {%
    \footnotesize\addfontfeature{Color=lightgray}\itshape%
    \usebibmacro{journal+issuetitle}%
    \setunit{\space}%
    \printfield{pages}%
    \newunit%
    \printlist{publisher}%
    \setunit*{\addcomma\space}%
    \printfield{year}%
    \newunit%
  }
  \par\vspace{0.3\baselineskip}
}

\DeclareBibliographyDriver{inproceedings}{%
  \printfield{title}%
  \newblock%
  \printnames{author}%
  \par%
  \newblock%
  {%
    \footnotesize\addfontfeature{Color=lightgray}%
    \printfield{booktitle}%
    \setunit{\addcomma\space}%
    \printfield{year}%
    \setunit{\addcomma\space}%
    \printlist{location}%
    \newunit%
  }
  \par\vspace{0.3\baselineskip}
}

\DeclareBibliographyDriver{misc}{%
  \printfield{title}%
  \newblock%
  \printnames{author}%
  \par%
  \newblock%
  {%
    \footnotesize\addfontfeature{Color=lightgray}\itshape%
    \printfield{booktitle}%
    \setunit*{\addcomma\space}%
    \printfield{note}%
    \setunit*{\addcomma\space}%
    \printfield{year}%
    \setunit{\addcomma\space}%
    \printlist{location}%
    \newunit%
  }
  \par\vspace{0.3\baselineskip}
}

\DeclareBibliographyDriver{report}{%
  \printfield{title}%
  \newblock%
  \printnames{author}%
  \par%
  \newblock%
  {%
    \footnotesize\addfontfeature{Color=lightgray}\itshape%
    \printfield{type}%
    \setunit{\space}%
    \printfield{number}%
    \setunit{\addcomma\space}%
    \printfield{year}%
    \newunit%
  }
  \par\vspace{0.3\baselineskip}
}

\DeclareNameFormat{author}{%
  \small\addfontfeature{Color=lightgray}%
  \ifblank{#3}{}{#3\space}#1%
  \ifthenelse{\value{listcount}<\value{liststop}}
    {\addcomma\space}
    {}%
}

\newcommand{\printbibsection}[2]{
  \begin{refsection}
    \nocite{*}
    \printbibliography[sorting=chronological, type={#1}, title={#2}, heading=subbibliography]
  \end{refsection}
}

\DeclareSortingScheme{chronological}{
  \sort[direction=descending]{\field{year}}
  \sort[direction=descending]{\field{month}}
}

%%%%%%%%%%%%%%%%
% Other tweaks %
%%%%%%%%%%%%%%%%

\RequirePackage[left=1.5cm,top=1.5cm,right=1.5cm,bottom=2.5cm,nohead]{geometry}
\RequirePackage{hyperref}
\RequirePackage{longtable}
\RequirePackage{fancyhdr}
